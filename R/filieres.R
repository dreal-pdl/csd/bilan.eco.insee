#' filieres
#'
#' Libelle des filiere du dataset ODRE 'prod-region-annuelle-enr'.
#'
#' @format A data frame with 7 rows and 2 variables:
#' \describe{
  #'   \item{ indicateur }{  factor, libelle des champs }
  #'   \item{ filiere }{  factor, nom de la filiere en bon francais }
  #' }
  #' @source ODRE
  "filieres"
