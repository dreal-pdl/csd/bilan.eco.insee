---
title: "Exploration"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{exploration}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE, }
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  message = FALSE,
  warning = FALSE
)

ggplot2::theme_set(gouvdown::theme_gouv(plot_title_size = 12, subtitle_size  = 11, base_size = 9, caption_size = 9) +
                     ggplot2::theme(plot.caption.position =  "plot", legend.position = 'bottom', 
                                    legend.margin = ggplot2::margin(t = 0, r = 0, b = 0, l = 0, unit = "pt"),
                                    legend.box.margin = ggplot2::margin(t = 0, r = 0, b = 0, l = 0, unit = "pt"),
                                    plot.margin = ggplot2::margin(t = 1, r = 0.15, b = 0.15, l = 0.15, unit = "pt"),
                                    plot.caption = ggplot2::element_text(colour = "grey40")))

flextable::set_flextable_defaults(font.size = 10, font.family = "Marianne", padding.bottom = 1, table.layout = "autofit")
```

```{r setup}

library(bilan.eco.insee)
params <- list(mil = 2023, reg = "52")

```

Ce package rassemble et produit les éléments nécessaires à la rédaction d'une partie 'énergie' d'un bilan économique et social annuel régional. 
IL permet de produire un canevas de publication à commenter à partir de 2 paramètres : l'année observée et le code région (au format texte = entre "").   
  
# Climat
  
Les conditions climatiques de l'année écoulée sont l'un des facteurs explicatifs des évolutions de consommation d'énergie et de production renouvelable (éolien, solaire).  
  
## Téléchargement et mise en forme des DJU indice de rigueur  
  
Le SDES publie les degrés jour unifiés (DJU) annuels nationaux, régionaux et départementaux qui permettent de construire un indice de rigueur climatique.   
  
L'indice de rigueur climatique est le rapport entre un indicateur de climat observé et un indicateur de climat de référence (période trentenaire). Cet indicateur est constitué par des degrés jours unifiés (écart journalier entre la température observée et 17°C). Par convention le degré jour unifié est égal à zéro si la température observée est supérieure ou égale à la température de référence.  
  
[Lien vers la rubrique.](https://www.statistiques.developpement-durable.gouv.fr/indice-de-rigueur-degres-jours-unifies-aux-niveaux-national-regional-et-departemental?rubrique=23&dossier=189)  
  
**Calcul DJU**
  
Pour chaque jour de l'année, on compare la température observée à un seuil, fixé à 17°C au SDES. Plus précisément, on calcule T, moyenne des extrêma des températures sur une journée :  
T = (T minimum + T maximum) / 2   
Le nombre de degrés-jours de cette journée est égale à :  
• 17-T si T < 17°C,  
• à 0 sinon.  
  
On appelle degrés-jours unifiés, DJU, la somme des degrés-jours de tous les jours de la "saison de chauffe", période de l’année qui va par convention de janvier à mai et d’octobre à décembre.  
  
Il faut calculer une moyenne des DJU sur la période de référence, pour le SDES 1991-2020 (à vérifier dans le fichier national téléchargé, script adapté en 2023 pour faire cette vérification) .  
On ramène ensuite la DJU de l'année n sur celle de la période de référence.
Le ratio DJUn / DJU0 est appelé indice de rigueur de l'année n.  
  
Ainsi :  
• si l’indice est supérieur à 1, l’année considérée a été plus rigoureuse qu’une année moyenne  
• si l’indice est inférieur à 1, l’année considérée a été moins rigoureuse qu’une année moyenne.  


```{r DJU}

dju <- prep_dju(url_dju_reg = "2024-02/dju_anciennes_regions_1981_2023_0.xlsx", url_dju_nat = "2024-02/dju_1970_2023.xlsx", 
                reg = params$reg, an = params$mil, coeff_dju = 25)
```

  
## Recherche documentaire météo 2023
  
A côté de cette source de données, il est utile de rassembler et synthétiser quelques informations sur la météo observée durant l'année écoulée, à partir du [bilan climatique annuel de Météo France (France entière) et des bilan climatiques régionaux mensuels](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=129&id_rubrique=29).  


Bulletins France :  
https://donneespubliques.meteofrance.fr/donnees_libres/bulletins/BCM/202312.pdf  

Bulletins PDL :  
https://donneespubliques.meteofrance.fr/donnees_libres/bulletins/BCMR/BCMR_12_202312.pdf  
  

**Faits marquants Météo France 2023** :   

**Pays de la Loire** : 2023 est une année très chaude, tout comme en 2022, mais avec un arrosage globalement excédentaire. L'ensoleillement, sans être exceptionnel, est au dessus de la normale.    
- ENSOLEILLEMENT : Il est partout supérieur à la normale (de 7 à 11 %);  
- PLUVIOMETRIE : Les pércipitations sont quasiment partout excédentaires;  
  
**France** : Deuxième année la plus chaude depuis 1900 derrière 2022 : la France a connu l'automne le plus chaud depuis 1900.    
   
# Consommation électrique et gaz : 

##  Recherche documentaire  
 
RTE publie fin février son bilan annuel national sur [son site internet](https://www.rte-france.com/analyses-tendances-et-prospectives/bilans-electriques-nationaux-et-regionaux). Il fournit les messages clés sur la consommation et la production d'électricité à l'échelle nationale.  
Le bilan régional n'est publié qu'en septembre [son site internet](https://www.rte-france.com/analyses-tendances-et-prospectives/bilans-electriques-nationaux-et-regionaux#Lesdocuments).  

### Fait marquants RTE bilan national électrique 2023 :  
- la production électrique de toutes les filières décarbonées a nettement progressé (nucléaire, hydraulique, éolien, solaire)   
- la consommation a diminué par rapport à l’année précédente, facilitant la couverture de la demande, dans la continuité de la dynamique observée à l’automne 2022   
- La conjonction simultanée de baisse de la demande et de hausse de la production décarbonée ont concouru à diminuer le recours aux combustibles fossiles et en particulier au gaz (dont la production est passée de 44,1 TWh en 2022 à 30,0 TWh en 2023).  
  
**La consommation a affiché un net recul qui confirme la tendance amorcée en 2022**     
  
En 2023, la consommation d’électricité en France, corrigée des effets météorologiques et calendaires, a représenté 445,4 TWh, soit un recul de 3,2 % par rapport à l’année précédente, où la consommation avait déjà atteint un creux de 460,2 TWh du fait de la crise énergétique.
  
Par ailleurs, la baisse de consommation entre 2022 et 2023 est une des plus fortes jamais constatées (-3,2 %) : plus conséquente que celles observées entre 2021 et 2022 (-1,1 %), et entre 2008 et 2009 à la suite de la crise économique (-1,5 %), et très proche de la baisse de consommation qui avait eu lieu entre 2019 et 2020 (-3,7 %).   
  
La réalisation d’une enquête en partenariat avec l’institut IPSOS a permis de montrer que cette diminution n’était pas uniquement le résultat de démarches de sobriété volontaires (d'une part, une mobilisation continue en faveur des économies d’énergie dans les administrations, les entreprises et de la part des ménages, avec le lancement d’un second plan gouvernemental en octobre 2023) mais découle également d’une réaction de la population et des acteurs économiques vis-à-vis de la hausse de prix dans l’ensemble de l’économie (effet de l'inflation persistante 4,9 % en 2023 contre 5,2 % en 2022 selon l’INSEE).  
   
Les températures élevées en 2023, deuxième année la plus chaude jamais enregistrée en France, ont tiré la consommation « brute » (sans correction des effets du climat) davantage à la baisse.   
  
**la production électrique de toutes les filières décarbonées a nettement progressé**
  
Essentiellement sous l’effet de la crise de la production nucléaire due au phénomène de corrosion sous contraintes, mais également en partie du fait des faibles précipitations, l’année 2022 avait été singulière pour la production d’électricité en France, autant du point de vue du volume total produit que de la répartition entre filières. En 2023, la production a retrouvé des caractéristiques plus proches de l’historique, tout en restant en écart par rapport aux années avant crise. Ainsi, le volume de production a nettement progressé en 2023 (+11 %) par rapport au niveau historiquement bas de l’année 2022, atteignant 494,3 TWh (contre 445,5 TWh en 2022).   

  
L’année 2023 a été caractérisée par des records de production à la fois pour l’éolien (50,7 TWh) et le solaire (21,5 TWh), qui ensemble ont totalisé près de 15 % du mix de production électrique, contribuant ainsi à la sécurité d’approvisionnement et à l’augmentation de l’offre d’électricité bas-carbone en France et dans les pays voisins, grâce aux échanges. En 2023, la capacité installée du parc solaire et éolien en mer a progressé de manière inédite pour la France. Le volume d’énergie produite par les parcs offshore a représenté 1,9 TWh en 2023. C’est principalement le parc de Saint-Nazaire qui en est à l’origine, le volume de production des deux nouveaux parcs ayant été faible sur l’année du fait d’une mise en service progressive.  
  
La production hydraulique (58,8 TWh), qui est restée la deuxième filière de production électrique, a connu une nette reprise par rapport à l’année 2022 grâce notamment à des précipitations plus abondantes, qui ont permis de garder des niveaux de stock élevés.  

Dans l'ensemble, filières bas-carbone ont continué de dominer largement le mix électrique : la production d’électricité française se situe parmi les moins émissives en gaz à effet de serre en Europe, un constat qui s’est confirmé en 2023 avec près de 92 % de production décarbonée. Ces éléments ont conforté la place du système électrique national en tant qu’atout de décarbonation de l’électricité en Europe grâce à l’exportation d’une partie de sa production bas-carbone.   

  
### Fait marquants RTE bilan national gaz 2023 :  

https://www.grtgaz.com/medias/communiques-de-presse/bilan-gaz-2023  

La consommation de gaz en France en baisse de 11,4% par rapport à 2022 :   
- Rupture tendancielle de la consommation à partir de 2021 reflétant un changement de comportement des consommateurs (efforts de sobriété et des réponses à l’inflation initiées au 4 ème trimestre 2022, avec le plan de sobriété et la charte écogaz et maintenues tout au long de 2023) et un climat doux (2023 deuxième année la plus chaude après 2022 depuis 1900)  
- Baisse des consommations finales de gaz (-6,7%) dont distributions publiques (-6,3%) et industriels (-7,2%) vs. 2022  
- La consommation des clients industriels raccordés au réseau de GRTgaz recule quant à elle de 7,4 % à 103,8 TWh (-18,2 % depuis 2021). Dans chaque secteur, l’évolution de la consommation des industriels est la résultante de 3 effets principaux : évolution de l’activité industrielle, efforts d’efficacité énergétique et substitution entre énergies.  
- Moindre sollicitation de la production électrique centralisée à partir de gaz (-40%) après une année 2022 exceptionnelle marquée par une forte indisponibilité des centrales nucléaires  
- biométhane : 652 sites de méthanisation injectent dans les réseaux gaziers à fin 2023 (+138 ), dont 80 dans le réseau de GRTgaz ( (+17 ) représentant près de 20% de la capacité
nationale. Dans ces conditions, l’ambition nationale de 44 TWh de gaz renouvelables injectés dans les réseaux français pourra être atteinte en 2030.  
   
## Téléchargement des données depuis la plate-forme ODRE  
  
Les données sont récupérées fin février sur la plateforme opendata.reseaux-energies.fr.  
source : [GRTgaz, RTE, Teréga](https://opendata.reseaux-energies.fr/explore/dataset/consommation-annuelle-brute-regionale/)   
  
S'il est mis à jour, il peut être intéressant de regarder le tableau de bord régional qui donne quelques chiffres clés: [Tableau de bord régional](https://opendata.reseaux-energies.fr/tdb-regional/) : exemple la consommation par habitant et un classement par région les plus énergivores et aussi le secteur le plus consommateur...   
  
```{r consommation}
conso <- dataset_odre(x = "consommation-annuelle-brute-regionale")
```
Les données pour le gaz concernent la France continentale (= sans la Corse)  

## Viz

```{r consommation viz, fig.width=7.5, fig.height=5, warning=FALSE}
conso_1 <- prep_conso(dataset_conso = conso$dataset, reg = params$reg, an = params$mil, coeff_fce_reg = 1/17)
conso_2 <- finit_conso(conso_1, dju)

viz_conso(conso_2)

```
 
## Table d'analyse des consommations anuelles
```{r table analyses}
tble_conso2 <- table_conso(df = conso_2, an = params$mil)
table_conso_viz(df = tble_conso2)
```


# Production renouvelable biométhane et électricité


## Recherches documentaires  


**Objectifs régionaux SRADDET**  
Exprimé en volume d'électricité produite (GWh)

 | Filière | 2030 | 2050  |
 | ------- | ---- | ----- | 
 | Eolien  | 4500 |  6000 | 
 | Hydraulique |  25 | 30 | 
 | Photovoltaïque	| 2000 | 
 | Hydraulique | 2050 |  5200 |  



**Objectifs nationaux 2023**  
Source : Programmation pluriannuelle de l'énergie (PPE) - Valeurs à retrouver dans :  

* les [Chiffres clés des énergies renouvelables - Édition 2021](https://www.statistiques.developpement-durable.gouv.fr/edition-numerique/chiffres-cles-energies-renouvelables-2021/4-objectifs-dans-le-cadre-de), pour la puissance installée et   
* le [Plan national intégré énergie-climat de la France de mars 2020, page 206](https://www.ecologie.gouv.fr/sites/default/files/PNIEC_France_mars_2020.pdf) pour la production correspondante.  

* 24,1 GW d’éolien terrestre en 2023  -  52 TWh  
* 20,1 GW de photovoltaïque fixé par la PPE pour 2023 - 24 TWh  
* 25,7 GW pour l'hydro-électricité (stable) - 62 TWh  
* injection biométhane : 6 TWh  
Ajout :  
* éolien en mer : 2,4 GW - 9 Twh  

**Projet de mise à jour (octobre 2023) du PLAN NATIONAL INTEGRE ENERGIE-CLIMAT DE LA FRANCE**  
  
Le Projet de mise à jour du [Plan national intégré énergie-climat de la France, page 8](https://www.actu-environnement.com/media/pdf/news-42985-projet-pniec-2023.pdf).   

Cibles renouvelables nationale par filière En 2030 dans ce projet :  
* PV : 54 à 60 GW  
* Eolien terrestre : 33 à 35 GW  
* Eolien en mer : 3,6 GW  
* Hydroélectricité (dont STEP) : 26,3 GW  
* Chaleur renouvelable et froid renouvelable : 297 TWh  
* Biocarburants : 48 TWh  
* Biogaz : 50 TWh   

**Attente de la PPE 3 (2024-2033)**  


**Autres outils réglementaires**  
De nouveaux outils réglementaires pour accélérer le développement des gaz renouvelables :
- 10 juin 2023 : réévaluation du tarif d’achat  
(méthanisation inférieure à 25 GWh/an de
- 15 février 2024 : échéance de l’appel d’offre biométhane en attente de la déclaration des lauréats  
- Publication imminente de la trajectoire d’obligation de Certificats de Production de Biogaz   
- Lancement prochain d’un premier appel à projet pour soutenir la pyrogazéification  

## Téléchargement du jeu de données  

[source production : GRTgaz, RTE, Teréga](https://opendata.reseaux-energies.fr/explore/dataset/prod-region-annuelle-enr/)  

Ce jeu de données présente les productions régionales annuelles d'énergie renouvelable en France (GWh) pour l'électricité depuis 2008 et depuis 2015 pour le gaz.  
Pour le biométhane, il s'agit des quantités injectées dans les réseaux de gaz (réseaux de distribution et de transport) en France comptabilisées aux points d'interfaces transport/distribution. Les injections sont donc affectées à la région du point de comptage et non à celle du site lorsque celle ci est différente.  
Selon l'arrêté du 8 novembre 2007 pris en application de l'article 2 du décret n° 2006-1118 du 5 septembre 2006 relatif aux garanties d'origine de l'électricité produite à partir de sources d'énergie renouvelables ou par cogénération » , la partie renouvelable est calculée comme suit :   
* Hydraulique : On soustrait 70 % de la consommation liée au pompage.  
* Bioénergies : La production des usines d'incinération d'ordures ménagères est comptabilisée à 50%. Les autres combustibles sont comptabilisés à 100 %   
* Eolien et solaire : La production est comptabilisée à 100 %.   


```{r prodenr}
prod_enr <- dataset_odre(x = "prod-region-annuelle-enr")
```


## Visualisations

```{r prodenr viz, fig.width=7.5, fig.height=5, message=FALSE}
prod_enr_viz <- prep_prod(dataset = prod_enr$dataset, region = params$reg)

viz_prod(prod_enr_viz)
```

## Tables d'analyses   

Les objectifs nationaux et régionaux, exprimés en TWh d'électricité produite, à prendre en compte dans le tableau d'analyse sont chacun paramétrables par une liste.  
  
Electricité renouvelable = bio-énergie + éolien + hydro-électricité + solaire photovoltaïque   
Toutes Enr de réseau = electricité renouvelable + injections de biométhane    

```{r table analyses prod elec}
tble_prod_elec2 <- table_prod(prod_enr_viz, an = params$mil, 
                              obj_reg = list(eol = NA_real_, pv = NA_real_, hydro = NA_real_, elec = NA_real_), 
                              obj_nat = list(eol = 52, pv = 24, hydro = 62, elec = 155))
table_prod_viz(tble_prod_elec2)
```

Production d'une table séparée pour le biogaz car la filière a démarré plus tardivement.  


```{r table analyses prod gaz}
tble_prod_biogaz <- table_biogaz(prod_enr_viz, an = params$mil)
table_biogaz_viz(tble_prod_biogaz)
```

L'objectif national est d'injecter 6 TWh de biométhane sur le réseau d'ici à 2023.  

